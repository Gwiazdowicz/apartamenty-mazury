<footer class="footer">
    <div class="footer__border">
        <div class="footer__container">
            <div class="footer__part-top">
                <div class="footer__box">
                    <div class="footer__box-title">Menu</div>
                    <div><a  href="#!" class="footer__box-subtitle">Strona główna</a></div>
                    <div><a  href="#!" class="footer__box-subtitle">O nas</a></div>
                    <div><a  href="#!" class="footer__box-subtitle">Ważne informacje</a></div>
                    <div><a  href="#!" class="footer__box-subtitle">Promocje</a></div>
                    <div> <a  href="#!" class="footer__box-subtitle">Kontakt</a></div>
                </div>

                <div class="footer__box">
                    <div class="footer__box-title">DANE KONTAKTOWE</div>
                    <div class="footer__box-description">Mazury Apartamenty ul. Warszawska 20/10 02-200 Mikołajki</div>
                    <div class="footer__box-description"><a href="tel:+48713333222">Tel. +48 200 300 400</a><br> E-mail:<span class="footer__description--white"> <a href ="mailto: kontakt@ma.pl">kontakt@ma.pl</a></span></div>
                    
                </div>

                <div class="footer__box">
                    <div class="footer__box-title">Obserwuj nas</div>
                    <div class="footer__icons-box">
                        <a href="#!"><div class="footer__icon footer__icon--one"></div></a>
                        <a href="#!"><div class="footer__icon footer__icon--two"></div></a>
                    </div>
                </div>

                <div class="footer__box">
                    <div class="footer__box-title">FORMULARZ KONTAKTOWY</div>
                    <form>
                        <div class="footer__row">
                            <div class="footer__input-box">
                                <div class="footer__input"><input type="text"  placeholder="Imię i Nazwisko*"></div>
                                <div class="footer__input"> <input type="email"  placeholder="E-mail*"></div>
                                <div class="footer__input"> <input  type="number"  placeholder="Telefon*"></div>
                            </div>
                            
                            <div class="footer__textarea"><textarea placeholder="Wiadomość"></textarea></div>
                        </div>    
                            <div class="footer__btn-box">
                                <a href="!#"><div class="footer__btn"></div></a>
                            </div>
                    </form>
                </div>
            </div>

            <div class="footer__part-bottom">
                <div  class="footer__bottom-block">
                    <a href="#!" class="footer__title-last">2018 © KajWare </a>
                </div>
            </div>
        </div>
    </div>
</footer>


</body>
</html>
