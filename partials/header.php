<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>App Title</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Check if JS is enabled, remove html "no-js" class    -->
  <script>(function (html) {
      html.className = html.className.replace(/\bno-js\b/, 'js')
    })(document.documentElement);</script>
    
    <?php Utils::includeStyles(); ?>
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">        
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCPhpKSH89psd3h29vQ_nct7Sd9fjxTpLM&callback=initMap"
    ></script>
    <script src="parsley.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" type="text/javascript" charset="utf-8"></script>
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,500,600,700,800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">

    <script>
    var map;
    function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 53.798, lng: 21.502},
        zoom: 11
    });

    var marker = new google.maps.Marker({
          position: {lat: 53.798, lng: 21.502},
          map: map,
          title: 'Hello World!',
          icon: "_src/dev-assets/img/marker-2.png"
    });
    var marker = new google.maps.Marker({
        position: {lat: 53.850, lng: 21.502},
        map: map,
        icon: "_src/dev-assets/img/marker-1.png"
    });  

    var marker = new google.maps.Marker({
        position: {lat: 53.850, lng: 21.680},
        map: map,
        icon: "_src/dev-assets/img/marker-4.png"
    });  

    var marker = new google.maps.Marker({
        position: {lat: 53.750, lng: 21.3432},
        map: map,
        icon: "_src/dev-assets/img/marker-5.png"
    });  

    var marker = new google.maps.Marker({
        position: {lat: 53.780, lng: 21.3432},
        map: map,
        icon: "_src/dev-assets/img/marker-6.png"
    });  


    var marker = new google.maps.Marker({
        position: {lat: 53.780, lng: 21.54},
        map: map,
        icon: "_src/dev-assets/img/marker-7.png"
    }); 

    var marker = new google.maps.Marker({
        position: {lat: 53.76, lng: 21.54},
        map: map,
        icon: "_src/dev-assets/img/marker-8.png"
    }); 

    var marker = new google.maps.Marker({
        position: {lat: 53.750, lng: 21.402},
        map: map,
        icon: "_src/dev-assets/img/marker-3.png",
        animation: google.maps.Animation.BOUNCE
    }); 

    }
</script>

    
    <?php Utils::includeScripts(); ?>
    
</head>
<body class="<?= Utils::bodyClass() ?>">



<!--[if lt IE 11]>
<p>You are using a very old version of Internet Explorer. Please consider updating.</p>
<![endif]-->
    <div class="header">
        <div class="header">
            <div class="header__top"></div>
            <div class="header__box">
                <a href="http://localhost:3000/apartamenty-mazury/">
                    <div class="header__logo">
                        <img src="_src/dev-assets/img/logo.png" alt="main-logo">
                    </div>
                </a>
                <div class="icon">
                    <div class="hamburger"></div> 
                </div>
                <div class="header__menu">
                    <div class="header__term">
                        <p class="header__text">Wybierz termin :</p>
                        <div class="header__term-box">
                            <div class="header__term-icon"></div>
                            <p class="header__data">15.03 - 20.03</p>
                        </div>
                    </div>
                    <div class="header__reserve-btn">
                        <p class='header__btn-text'>Rezerwuj<br>Apartament</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <aside class="off">
        <nav class="nav-list">
            <ul>
                <li><a href="http://localhost:3000/apartamenty-mazury/subpage">Apartamenty</a></li>
                <li><a href="http://localhost:3000/apartamenty-mazury/reservation">O nas</a></li>
                <li><a href="#!">Wazne informacje</a></li>
                <li><a href="#!">Promocje</a></li>
                <li ><a class="last-item" href="#!">Kontakt</a></li>
            </ul>
        </nav>
    </aside>





