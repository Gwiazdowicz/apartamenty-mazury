<?php
error_reporting(E_ALL);

if (BASE === '%BASE%') {
    echo '<h1>Your app base address is not set up.</h1>';
    echo '<ol>';
    echo '<li>Please open up .env file in root folder and provide app base url</li>';
    echo '<li>In root folder, run in terminal: php core/set-base.php</li>';
    echo '</ol>';
    die();
}

/*
 * App constants 
 * */
require_once TEMPLATE_DIR . 'core/constants.php';


/*
 * Router logic 
 * */
require_once TEMPLATE_DIR . 'core/Router.php';


/*
 * Utils and stuff 
 * */
require_once TEMPLATE_DIR . 'core/Utils.php';


/*
 * App routes 
 * */
require_once TEMPLATE_DIR . 'routes/routes.php';
