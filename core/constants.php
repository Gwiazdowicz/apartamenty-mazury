<?php
//server
const VIEWS    = TEMPLATE_DIR.'views/';
const PARTIALS = TEMPLATE_DIR.'partials/';

//browser
const IMAGES   = 'assets/img/';
const SCRIPTS  = 'assets/js/';
const STYLES   = 'assets/css/';

