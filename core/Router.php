<?php


class Router
{
    private $base;
    private $routes;

    public function __construct($routes, $base = '/')
    {
        $this->routes = $routes;
        $this->base   = $base;
    }


    public function resolve($request)
    {
        $route = str_replace($this->base, '', $request);
        $this->matchRoute($route);
    }


    private function getTemplate($path)
    {
        $path = VIEWS.$path;

        if ( ! file_exists($path)) {
            echo '<h1>Template file not found</h1>'.$path;
            die();
        }

        ob_start();
        
        Utils::getHeader();
        require_once($path);
        Utils::getFooter();

        ob_get_flush();

        die();
    }


    private function matchRoute($route)
    {
        if ( ! array_key_exists($route, $this->routes)) {
            http_response_code(404);
            $this->getTemplate('404.php');
        }

        http_response_code(200);
        $this->getTemplate($this->routes[$route]);
    }
}
