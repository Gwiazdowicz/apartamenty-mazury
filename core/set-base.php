<?php

$env = file_get_contents(__DIR__.'/../.env');
$app_base = explode('=', $env)[1];
$app_base = trim($app_base);

$htaccess = file_get_contents(__DIR__.'/../.htaccess');
$htaccess = str_replace(array('%BASE%', '#'), array($app_base, ''), $htaccess);
file_put_contents(__DIR__.'/../.htaccess', $htaccess);

$index = file_get_contents(__DIR__.'/../index.php');
$index = str_replace('%BASE%', $app_base, $index);
file_put_contents(__DIR__.'/../index.php', $index);
