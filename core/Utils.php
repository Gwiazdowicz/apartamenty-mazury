<?php


abstract class Utils
{
    public static function getHeader()
    {
        require_once PARTIALS.'header.php';
    }


    public static function getFooter()
    {
        require_once PARTIALS.'footer.php';
    }


    public static function includeStyles()
    {
        if (file_exists(STYLES.'style.min.css')) {
            echo '<link rel="stylesheet" href="assets/css/style.min.css">';
        }
        if (file_exists(STYLES.'style.css')) {
            echo '<link rel="stylesheet" href="assets/css/style.css">';
        }
    }


    public static function includeScripts()
    {
        if (file_exists(SCRIPTS.'main.min.js')) {
            echo '<script src="assets/js/main.min.js" defer></script>';
        }
        if (file_exists(SCRIPTS.'main.js')) {
            echo '<script src="assets/js/main.js" defer></script>';
        }
    }


    public static function bodyClass()
    {
        $request = $_SERVER['REQUEST_URI'];
        $name    = str_replace(BASE.'/', '', $request);

        if ($name == '') {
            $name = 'index';
        }

        return $name;
    }

}
