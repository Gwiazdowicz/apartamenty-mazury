import slick from 'slick-carousel';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import {Loader, LoaderOptions} from 'google-maps';
//import {CountUp} from "countup.js";
import {parsley} from "parsleyjs";

$(function () {
  $('#form').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
    $('#parsley-id-multiple-checkbox-1 .parsley-required').html("Zaznaczenie tego pola jest wymagane*");
    $('#parsley-id-multiple-checkbox-2 .parsley-required').html("Zaznaczenie tego pola jest wymagane*");
    $('#parsley-id-5 .parsley-required').html("To pole jest wymagane*");
    $('#parsley-id-5 .parsley-length').html("Podaj wartość od 3 do 20 znaków");
    $('#parsley-id-7 .parsley-required').html("To pole jest wymagane*");
    $('#parsley-id-7 .parsley-type').html("Podaj prawidłowy adres email");
    $('#parsley-id-9 .parsley-required').html("To pole jest wymagane*");
    $('#parsley-id-9 .parsley-type').html("Podaj prawidłowy numer telefonu");

  })
  .on('form:submit', function() {
 
  });
});

$("#number1").text(0);
$("#number2").text(0);
$("#number3").text(0);

$(window).scroll(function() {
  var hT = $('.about__block-bottom').offset().top,
      hH = $('.about__block-bottom').outerHeight(),
      wH = $(window).height(),
      wS = $(this).scrollTop();
  if (wS > (hT+hH-wH+1000)){
    $("#number1").text(0);
    $("#number2").text(0);
    $("#number3").text(0);
    console.log("Nizej")
  }

  if (wS < (hT+hH-wH)){
    $("#number1").text(0);
    $("#number2").text(0);
    $("#number3").text(0);
    console.log("Nizej")
  }
});


$(window).scroll(function (event) {
  var scroll = $(window).scrollTop();
  
  if(scroll > 800){
    $('.counter').each(function() {
      var $this = $(this),
          countTo = $this.attr('data-count');
      
      $({ countNum: $this.text()}).animate({
        countNum: countTo
      },
      {
        duration: 1000,
        easing:'linear',
        step: function() {
          $this.text(Math.floor(this.countNum));
        },
        complete: function() {
          $this.text(this.countNum);
        }
      });  
    });
  }
});


$(document).ready(function() {
  $('.icon').click(function() {
    $('.icon').toggleClass('active');
    $('aside').toggleClass('off');
  });
});

$('#vat').click(function() {
  if ($('#vat').is(':checked')) {
    $('.reservation__input-nip').slideDown('slow');
  } else {
    $('.reservation__input-nip').slideUp('slow');
  }
});

$('#comments').click(function() {
  if ($('#comments').is(':checked')) {
    $('.reservation__textarea').slideDown('slow');
  } else {
    $('.reservation__textarea').slideUp('slow');
  }
});

$('#textarea').click(function() {
  if ($('#textarea').is(':checked')) {
    $('.reservation__input-comments').slideDown('slow');
  } else {
    $('.reservation__input-comments').slideUp('slow');
  }
});

$('#radiobutton-one').click(function() {
  $('#paybox-2').removeClass('reservation__tile--active');
  if ($('#paybox-1').hasClass('reservation__tile--active')) {
    $('#paybox-1').removeClass('reservation__tile--active');
    $('#alert-pay').slideUp('slow');
    $('#radiobutton-one').prop("checked", false);  
  } else {
    $('#paybox-1').addClass('reservation__tile--active');
    $('#alert-pay').slideDown('slow');
  }
});

$('#radiobutton-two').click(function() {
  $('#alert-pay').slideUp('slow');
  $('#paybox-1').removeClass('reservation__tile--active');
  if ($('#paybox-2').hasClass('reservation__tile--active')) {
    $('#paybox-2').removeClass('reservation__tile--active');
    $('#radiobutton-two').prop("checked", false); 
  } else {
    $('#paybox-2').addClass('reservation__tile--active');
  }
});


const $actionsContainer = $('.slider-container__action');

const $slider = $('.slick .slider').slick({
  autoplay: true,
  autoplaySpeed: 3000,
  initialSlide: 0,
  arrows:false
});

$actionsContainer.find(`[data-slide="1"]`).css('opacity', 1);

$slider.on('afterChange', function (slick, e, currentSlide) {
  $actionsContainer.find(`[data-slide]`).css('opacity', 0.30);
  $actionsContainer.find(`[data-slide="${currentSlide + 1}"]`).css('opacity', 1);
});

$actionsContainer.on('click', 'a[data-slide]', function () {
  const slideNumber = $(this).attr('data-slide') - 1;
  $slider.slick('slickGoTo', slideNumber);
});































