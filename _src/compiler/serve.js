const path = require('path');
const fs = require('fs-extra');

const webpack = require('webpack');
const browserSync = require('browser-sync').create();

const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');

const {
  context,
  outputFolder,
  publicFolder,
  proxyTarget,
  watch
} = require('./../project.config');
const webpackConfig = require('./webpack.config')({ dev: true });
const getPublicPath = require('./publicPath');

const compiler = webpack(webpackConfig);

const imgSrc = path.resolve(context, 'img');
const imgOutput = path.resolve(outputFolder, 'img');

fs.removeSync(outputFolder);
if (imgSrc) {
  fs.copy(imgSrc, imgOutput);
  
  /* 
    Watch for file changes in img folder.
    Rebuild if change (new/deleted). 
  */
  fs.watch(imgSrc, function() {
    fs.removeSync(outputFolder);
    fs.copy(imgSrc, imgOutput);    
  });
}

const middleware = [
  webpackDevMiddleware(compiler, {
    publicPath: getPublicPath(publicFolder),
    logLevel: 'silent',
    quiet: true
  }),
  webpackHotMiddleware(compiler, {
    log: false,
    logLevel: 'none'
  })
];

browserSync.init({
  middleware,
  open: false,
  proxy: {
    target: proxyTarget,
    middleware
  },
  logLevel: 'info',
  files: watch.map(element => path.resolve(element)),
  snippetOptions: {
    rule: {
      match: /<\/head>/i,
      fn: function(snippet, match) {
        return `<script defer="defer" src="${getPublicPath(
          publicFolder
        )}js/main.js"></script>${snippet}${match}`;
      }
    }
  },
  rewriteRules: []
});
