/*
 * Prettier global settings go here.
 * For project specific rules - please see .prettierrc.js
 **/

module.exports = {
  trailingComma: 'none',
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  printWidth: 80
};
