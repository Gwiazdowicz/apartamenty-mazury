<section class="hero">
    <div class="hero__background"></div>
        <div class="hero__border">
            <div class="hero__container">
                <div class="hero__content">
                    <h1 class="hero__title">Najlepsze apartamenty</h1>
                    <div class="hero__subtitle-box">
                        <div class="hero__arrow"></div>
                        <h2>Na Mazurach</h2>
                        <div class="hero__arrow"></div>
                    </div>
                </div>
            </div>
        </div>
</section>

<section class="apartments">
    <div class="apartments__border">
        <div class="apartments__container">
            <div class="apartments__box">
                <div class="apartments__tile apartments__tile--one">
                    <h2 class="apartments__tile-title">Luxusowy Apartament</h2>
                    <p class="apartments__tile-subtitle">Zeglarska 2</p>
                </div>

                <div class="apartments__tile apartments__tile--two">
                    <h2 class="apartments__tile-title">Luxusowy Apartament</h2>
                    <p class="apartments__tile-subtitle">Gizycko</p>
                </div>

                <div class="apartments__tile apartments__tile--three">
                    <h2 class="apartments__tile-title">Przestronny Apartament</h2>
                    <p class="apartments__tile-subtitle">Mikołajki</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="break">
    <div class="break__border">
        <div class="break__container">
            <div class="break__box">
                <div class="break__base"></div>
                <div class="break__logo break__logo--one"></div>
                <div class="break__base"></div>
            </div>
        </div>
    </div>
</section>

<section class="about">
    <div class="about__border">
        <div class="about__container">
            <div class="about__block">
                <div class="about__col-one">
                    <div class="about__box-left">
                        <h2 class="about__title">Witaj w Krainie Mazur</h2>
                        <p class="about__subtitle a-article">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat nesciunt eveniet pariatur facilis consequuntur? Unde aliquid dolore adipisci vitae, sapiente hic neque nostrum explicabo quaerat architecto iure quo cumque eos?</p>
                        <div class="about__btn-box">
                            <a href="#!">
                                <p>Zobacz więcej</p>
                                <div class="about__arrow"></div>
                            </a>
                        </div>
                    </div>
                    <div class="about__box-right">
                        
                    </div>
                </div>
            </div>  
            <div class="about__block-bottom">
                <div class="about__col-two">
                    <div class="about__tile">
                        <h3 id="number1" data-count="18" class="about__number counter">18</h3>
                        <p class="about__text">wyjątkowych apartamentów</p>
                        <div class="about__base"></div>
                    </div>
                    <div class="about__tile">
                        <h3 id="number2" data-count="300" class="about__number counter">300</h3>
                        <p class="about__text">metrów od jeziora</p>
                        <div class="about__base"></div>
                    </div>
                    <div class="about__tile">
                        <h3 id="number3" data-count="750" class="about__number counter">750</h3>
                        <p class="about__text">metrów od centrum</p>
                        <div class="about__base"></div>
                    </div>
                </div>
            </div>      
        </div>
    </div>
</section>


<section class="break">
    <div class="break__border">
        <div class="break__container">
            <div class="break__box">
                <div class="break__base"></div>
                <div class="break__logo break__logo--two"></div>
                <div class="break__base"></div>
            </div>
        </div>
    </div>
</section>


<section class="clients">
    <div class="clients__container">
        <div class="slick">
            <div class="slider slider-nav">
                <div class="clients__opinion">Jesteśmy bardzo zadowoleni z naszego apartamentu.<br>Pokój był bardzo duży i czysty.Łóżka niezwykle wygodne. <br>W okresie zimowym było cieplutko :).<br>Polecamy gorąco!</div>
                <div class="clients__opinion">Jesteśmy bardzo zadowoleni z naszego apartamentu.<br>Pokój był bardzo duży i czysty.Łóżka niezwykle wygodne. <br>W okresie zimowym było cieplutko :).<br>Polecamy gorąco!</div>
                <div class="clients__opinion">Jesteśmy bardzo zadowoleni z naszego apartamentu.<br>Pokój był bardzo duży i czysty.Łóżka niezwykle wygodne. <br>W okresie zimowym było cieplutko :).<br>Polecamy gorąco!</div>
            </div>
        </div>
            <div class="clients__action slider-container__action">
                <a data-slide="1" class=" clients__box slider-container__client-box-two" id="client-1">
                    <div class="clients__img clients__img--one slider-container__client-one"></div>
                    <div class="clients__data slider-container__client-data">Sławomir<br><span>Apartament Amelia</span></div>
                </a>
                <a data-slide="2" class=" clients__box slider-container__client-box-two" id="client-2">
                    <div class="clients__img clients__img--two slider-container__client-two"></div>
                    <div class="clients__data slider-container__client-data">Agnieszka i Mariusz<br><span>Apartament Żagiel</span></div>
                </a>
                <a data-slide="3" class="clients__box slider-container__client-box-three" id="client-3">
                    <div class="clients__img clients__img--three slider-container__client-three"></div>
                    <div class=" clients__data slider-container__client-data">Paulina<br><span>Apartament Gracja</span></div>
                </a>
            </div>
        </div>     
    </div>
</section>

<section class="location">
    <div class="location__border">
        <div class="location__container">
           <div class="location__block">
             <div class="location__row-one">
                <div id="map" class="location__map"></div>
             </div>
             <div class="location__row-two">
                <div class="location__header">
                    <h2 class="location__title" >Lokalizacje</h2>
                    <p  class="location__subtitle">Wybierz apartament z listy lub kliknij w znacznik na mapie</p>
                </div>
                <ol class="location__list">
                    <li class="location__list-element">Apartamenty w Gizycku</li>
                    <li class="location__list-element">Luksusowy Apartament w Gizycku</li>
                    <li class="location__list-element">Apartamenty Grecja w Mrągowie</li>
                    <li class="location__list-element">Luksusowy Apartament Vinnetto w Mikołajkach</li>
                    <li class="location__list-element">Apartamenty Amelia w Runie</li>
                    <li class="location__list-element">Luksusowy Apartament Grecja w MIkołajkach </li>
                    <li class="location__list-element">Apartament Zagiel w Mrogowie</li>
                    <li class="location__list-element">Apartament Gracjan w Mrągowie</li>
                </ol>  
                <div class="location__btn">
                    <a href="#!">
                        <p class="location__btn-text">Zobacz więcej</p>
                        <div class="location__arrow"></div>
                    </a>
                </div>
             </div>
           </div>
        </div> 
    </div>
</section>


   


  





