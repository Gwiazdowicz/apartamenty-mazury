<section class="filter">
    <div class="filter__border">
        <div class="filter__container">
            <div class="filter__content">
                <h1 class="filter__title a-title">Rezerwuj on-line</h1>
            </div>
            <div class="filter__content">
                <h3 class="filter__subtitle">Maecanes malesuada ultricies</h3>
            </div>
            <div class="filter__content">
                <p class="filter__text">Lorem ipsum dolor sit amet <span class="filter__text"> consectetur adipisicing elit. Neque,</span> tenetur soluta in magnam placeat itaque dolore quos explicabo. Ab quas placeat animi ipsum quia veritatis rem numquam atque labore recusandae. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem enim quasi culpa maxime? Temporibus dolorem nesciunt aliquam eos possimus quasi vitae Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quisquam, dolore voluptatum. Aspernatur culpa dolores ratione suscipit tempore, molestiae impedit blanditiis, iure doloremque non, voluptas itaque consectetur ex placeat ut fuga.</p>
            </div>
            <div class="filter__box">
                <div class="filter__row">
                        <div class="filter__name-box">
                        <p class="filter__name">Typ:</p>
                        <div class="filter__select-box">
                                <div class="filter__logo"></div>
                                <div class="filter__discription">Apartament</div>
                                <a href="#!" class="filter__arrow"></a>
                        </div>
                        </div>
                        <div class="filter__name-box">
                        <p class="filter__name">Termin:</p>
                        <div class="filter__select-box filter__select-box--secound">
                                <div class="filter__discription filter__discription--two">15.03 - 20.03</div>
                                <a href="#!" class="filter__arrow"></a>
                        </div>
                        </div>
                        <div class="filter__name-box filter__name-box--last">
                        <p class="filter__name filter__name--last">Liczba gości:</p>
                        <div class="filter__select-box filter__select-box--last">
                                <div class="filter__discription filter__discription--two">4</div>
                                <a href="#!" class="filter__arrow filtr__arrow-three"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="gallery">
    <div class="gallery__border">
        <div class="gallery__container">
            <div class="gallery__block">
                <div class="gallery__tile">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Apartament</div>
                        </div>
                        <div class="gallery__img"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 129 zł <span>/ noc</span></div>
                    </div>
                </div>
           
                <div class="gallery__tile">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>
            
                <div class="gallery__tile gallery__tab">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                <div class="gallery__tile gallery__tab">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                <div class="gallery__tile gallery__desk">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                <div class="gallery__tile gallery__desk">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                <div class="gallery__tile gallery__desk">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                <div class="gallery__tile gallery__desk">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

                

                <div class="gallery__tile gallery__desk">
                    <div class="gallery__img-box">
                        <div class="gallery__header">
                            <div class="gallery__header-icon"></div>
                            <div class="gallery__header-title">Jacht</div>
                        </div>
                        <div class="gallery__img gallery__img--two"></div>
                        <div class="gallery__arrows-box">
                            <a class="gallery__arrow" href="#!"></a>
                            <a class="gallery__arrow gallery__arrow--invert" href="#!"></a>
                        </div>
                    </div>
                    <div class="gallery__info">
                        <h2 class="gallery__info-title">Luxsusowy Apartament</h2>
                        <p class="gallery__info-discription">Zeglarska 2, Mikołajki</p>
                    </div>

                    <div class="gallery__info gallery__info--boxes">
                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--one"></div>
                            <div class="gallery__info-text">2 pokoje</div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--two"></div>
                            <div class="gallery__info-text">41m2 </div>
                        </div>

                        <div class="gallery__info-box">
                            <div class="gallery__info-icon gallery__info-icon--three"></div>
                            <div class="gallery__info-text">4 osoby</div>
                        </div>
                    </div>

                    <div class="gallery__info gallery__info--btns">
                        <a href="#!" class="gallery__btn">
                            <div class="gallery__btn-text">Więcej</div>
                            <div class="gallery__btn-arrow"></div>
                        </a>
                        <div class="gallery__btn gallery__btn--white"> 80 zł <span>/ noc</span></div>
                    </div>
                </div>

            </div>
        </div>   
    </div>
</section>