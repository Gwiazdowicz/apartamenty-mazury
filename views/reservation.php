<section class="reservation">
    <div class="reservation__border">
        <div class="reservation__container">
            <h1 class="reservation__title a-title">Podsumowanie rezerwacji</h1>
            <div class="reservation__wrapper">
                <div class="reservation__col-one">
                    <form id="form">
                        
                        <div class="reservation__box reservation__box--data">
                            <h2 class="reservation__subtitle">Podaj dane rezerwującego</h2>
                            <div class="reservation__block">
                                <div class="reservation__input-box"> 

                                    <div class="reservation__input"><input class="form-control" type="text" name="name" required="" placeholder="Imię i Nazwisko*"  data-parsley-length="[3, 20]" data-parsley-group="block1"></div>
                                    <div class="reservation__input"><input class="form-control" name="email" type="email" data-parsley-trigger="change" required="" placeholder="Adres e-mail*"></div>
                                    <div class="reservation__input"><input class="form-control" type="tel" id="phone" name="phone" data-parsley-type="digits" required="" data-parsley-length="[4, 12]" data-parsley-group="block1" placeholder="Numer telefon*"></div>
                                </div>

                                <div class='reservation__checkbox'>

                                    <input type="checkbox" id="vat" name="vat">
                                    <label for="vat">Chcę otrzymać fakturę VAT</label>

                                    <div class="reservation__input-nip "><input type="text"  placeholder="Wpisz NIP"></div>
                                
                                    <input type="checkbox" id="comments" name="comments">
                                    <label for="comments">Mam uwagi do rezerwacji (np. godzina przyjazdu, zyczenia)</label>

                                    <textarea class="reservation__textarea" placeholder="Wiadomość"></textarea>
                                
                                    <input type="checkbox" id="textarea" name="textarea">
                                    <label for="textarea">Dane gościa inne niz rezerwującego</label>

                                    <div class="reservation__input-comments"><input type="text"  placeholder="Imię i Nazwisko*"></div>
                                </div>
                            </div>
                        </div>

                        <div class="reservation__box reservation__box--payment">
                            <h2 class="reservation__subtitle reservation__subtitle--payment">Wybierz zabezpieczenie rezerwacji</h2>
                            <div class="reservation__payment-box">

                                <div id="paybox-1" class="reservation__tile reservation__tile--active">
                                    <input id="radiobutton-one" type="radio" class="reservation__radio" checked="checked">
                                    <div class="reservation__tile-top">

                                        <div id="price-text-1" class="reservation__box-two">
                                        <p>645zł</p>
                                        <span>do zapłaty teraz</span>
                                        </div>
                                        <div class="reservation__box-one">
                                            <div class="reservation__icon"></div>
                                            <div class="reservation__content reservation__content">
                                                <h2 id="tite-payment-1">Tradycyjny przelew bankowy</h2>
                                                <p>Kwotę przedpłaty należy przelać na podane konto.W przypadku braku wpłaty rezerwacja zostanie anulowana.</p>
                                            </div>
                                        </div>
                                    
                                    </div>
                                    <div id="alert-pay" class="reservation__tile-bottom">
                                        <div class="reservation__info">
                                            <p>Numer konta bankowego oraz tytuł przelewu otrzyamcie państwo w potwierdzeniu rezerwacji</p>
                                        </div>
                                    </div>
                                </div>

                                <div id="paybox-2" class="reservation__tile">
                                    <input id="radiobutton-two" type="radio" class="reservation__radio">
                                    <div class="reservation__tile-top">
                                        <div id="price-text-2" class="reservation__box-two">
                                        <p>645zł</p>
                                        <span>do zapłaty teraz</span>
                                        </div>
                                        <div class="reservation__box-one">
                                            <div class="reservation__icon"></div>
                                            <div class="reservation__content">
                                                <h2>Płatność online Polcard</h2>
                                                <p>Kwotę przedpłaty należy przelać na podane konto. W przypadku braku wpłaty.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reservation__tile-bottom">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="reservation__box reservation__box--checkbox">
                            <div class="reservation__checkbox-two">
                                <input type="checkbox" id="checkbox-1" name="checkbox-1"   required="" >
                                <label for="checkbox-1" >Zapoznałem się i akcetuję <a href="#!">regulamin rezerwacji</a> i <a href="#!">warunki aplikacji</a></label>
                                <input type="checkbox" id="checkbox-2" name="checkbox-2"   required="">
                                <label for="checkbox-2">Wyrazm zgodę na otrzymywanie informacji o aktualnych wydarzeniach i promocjach <a> [więcej]</a></label>
                            </div>
                            <p class='reservation__rodo'>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore fuga facilis impedit itaque deleniti eveniet unde, sed cupiditate saepe! Eos et aliquam quisquam eligendi accusamus amet itaque aperiam distinctio consequuntur!</p>
                        </div>

                        <div class="reservation__box reservation__box--btn">
                            <div class="reservation__btns">
                                <a href="#!" class="reservation__btn">
                                    <div class="reservation__arrow"></div>
                                    <p>Powrót</p>
                                </a>
                                
                                <div class="reservation__btn reservation__btn--orange">
                                    <p>Rezerwuj apartament</p>
                                    <div class="reservation__arrow reservation__arrow--orange"></div>
                                    <input type="submit" class="reservation__submit-btn btn btn-default" value="validate">
                                </div>
                                
                            </div>
                        </div>
                    </form>
                </div>

                <div class="reservation__col-two">
                    <div class="reservation__box reservation__box--particularly">

                        <div class="reservation__particularly-box">
                            <h2>Szczegóły rezerwacji</h2>
                        </div>

                        <div class="reservation__particularly-box reservation__particularly-box--two">
                            <div class="reservation__date-box">
                                <p>Pon, 28 maj 2018</p>
                                <span>od 16.00</span>
                            </div>
                            <div class="reservation__date-arrow"></div>
                            <div class="reservation__date-box">
                                <p>Śro, 30 maj 2018</p>
                                <span>od 12.00</span>
                            </div>
                        </div>

                        <div class="reservation__particularly-box reservation__particularly-box--three">
                            <div class="reservation__location">
                                <p>Apartamenty Mazury</p>
                                <span>ul. Krakowska 23</span><br>
                                <span class="reservation__person">Osoby dorosłe: 2</span>
                            </div>
                        </div>

                        <div class="reservation__particularly-box reservation__particularly-box--four">
                            <div class="reservation__particularly-col-one">
                                <p>Pokoje</p>
                                <p>Przedpłata</p>
                                <p>Na miejscu</p>
                                <p>Podatki i opłaty</p>
                            </div>
                            <div class="reservation__particularly-col-two">
                                <p>480zł</p>
                                <p>140zł</p>
                                <p>505zł</p>
                                <p>25zł</p>
                            </div>
                        </div>
                    
                        <div class="reservation__particularly-box reservation__particularly-box--five">
                            <div class="reservation__paddle"></div>
                            <div class="reservation__sum">
                                <p>Suma</p>
                            </div>

                            <div class="reservation__price">
                                <p>645zł</p>
                            </div>
                        </div>
                    </div>


                    <div class="reservation__box reservation__box--offer">
                        <div class="reservation__offer-box">
                            <div class="reservation__offer-img reservation__offer-img--one"></div>
                            <p class="reservation__offer-text">Natychmiastowe potwierdzenie <br>rezerwacji</p>
                        </div>
                        <div class="reservation__offer-box">
                            <div class="reservation__offer-img reservation__offer-img--two"></div>
                            <p class="reservation__offer-text">Bezpieczeństwo danych osobowych i karty płatniczej</p>
                        </div>
                        <div class="reservation__offer-box reservation__offer-box--last">
                            <div class="reservation__offer-img reservation__offer-img--three"></div>
                            <p class="reservation__offer-text">Najlepsze ceny<br>unikalne oferty</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



