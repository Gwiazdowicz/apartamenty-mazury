<?php
/*
|--------------------------------------------------------------------------
| This is the main app entry file
|--------------------------------------------------------------------------
|
| PLEASE DO NOT EDIT ANYTHING HERE
|
| If you need to edit:
|   * app routes => routes/routes.php
|   * partials => partials/
|   * views => views/
*/

const BASE = '/apartamenty-mazury';
const TEMPLATE_DIR = __DIR__.'/';

require_once TEMPLATE_DIR.'core/bootstrap.php';

$request = $_SERVER['REQUEST_URI'];

$router = new Router($routes, $base = BASE);
$router->resolve($request);




