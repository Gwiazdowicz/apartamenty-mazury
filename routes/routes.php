<?php
/*
|--------------------------------------------------------------------------
| Specify main app routes here
|--------------------------------------------------------------------------
|
| The format is as follows:
| routePath => filename.php
|
| * routePath - without trailing slash
| * filename.php - file placed in the views folder
*/
$routes = array(
    '/' => 'homepage.php',
    '/apartamenty' => 'apartments.php',
    '/subpage' => 'subpage.php',
    '/reservation' => 'reservation.php'
);
